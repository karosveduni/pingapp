﻿using System.Net;
using System.Net.NetworkInformation;

using CommunityToolkit.Mvvm.Input;

namespace PingApp.Model
{
	public partial class ResultPing(IPAddress address, PingOptions? options, IPStatus status, long roundtripTime, byte[] buffer)
	{
		public IPStatus Status { get; } = status;

		public IPAddress? Address { get; } = address;

		public long RoundtripTime { get; } = roundtripTime;

		public PingOptions? Options { get; } = options;

		public byte[]? Buffer { get; } = buffer;

		[RelayCommand]
#pragma warning disable CA1822 // Пометьте члены как статические
		private Task BufferToClipboardAsync(byte[] buffer)
#pragma warning restore CA1822 // Пометьте члены как статические
		{
			return Clipboard.SetTextAsync(string.Join(string.Empty, buffer));
		}
	}
}
