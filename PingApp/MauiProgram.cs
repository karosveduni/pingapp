﻿using CommunityToolkit.Maui;

using Microsoft.Extensions.Logging;

using Mopups.Hosting;
using Mopups.Services;

using PingApp.ViewModel;

using UraniumUI;

namespace PingApp
{
	public static class MauiProgram
	{
		public static IServiceProvider ServiceProvider { get; private set; } = null!;

		public static MauiApp CreateMauiApp()
		{
			var builder = MauiApp.CreateBuilder();
			builder
				.UseMauiApp<App>()
				.UseMauiCommunityToolkit()
				.UseUraniumUI()
				.UseUraniumUIMaterial()
				.ConfigureMopups()
				.ConfigureFonts(fonts =>
				{
					fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
					fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
				});


#if DEBUG
			builder.Logging.AddDebug();
#endif

			builder.Services.AddSingleton(MopupService.Instance);
			builder.Services.AddSingleton<MainPageViewModel>();
			builder.Services.AddMopupsDialogs();

			var app = builder.Build();

			ServiceProvider = app.Services;

			return app;
		}
	}
}
