﻿using System.Globalization;

using CommunityToolkit.Maui.Converters;

namespace PingApp.Converter
{
	public sealed class CheckBufferIsNullOrEmptyReturnStringConverter : BaseConverterOneWay<byte[], string>
	{
		public override string DefaultConvertReturnValue { get => ""; set => throw new NotImplementedException(); }

		public override string ConvertFrom(byte[] value, CultureInfo? culture)
		{
			return value is { Length: not 0 } ? $"Buffer.Length is {value.Length}  bytes" : "Buffer is empty";
		}
	}
}
