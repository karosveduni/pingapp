using Mopups.Pages;

namespace PingApp.View.Popup;

public partial class AboutPopupPage : PopupPage
{
	const string LinkGitLab = "https://gitlab.com/karosveduni";

	public AboutPopupPage()
	{
		InitializeComponent();
	}

	private async void ButtonView_Tapped(object sender, EventArgs e)
	{
		await Browser.OpenAsync(LinkGitLab);
	}
}