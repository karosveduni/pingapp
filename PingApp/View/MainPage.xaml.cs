﻿using PingApp.ViewModel;

using UraniumUI.Pages;

namespace PingApp.View
{
	public partial class MainPage : UraniumContentPage
	{
		public MainPage()
		{
			InitializeComponent();
			BindingContext = MauiProgram.ServiceProvider.GetService<MainPageViewModel>();
		}
    }

}
