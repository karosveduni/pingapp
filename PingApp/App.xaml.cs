﻿using System.Runtime.CompilerServices;

using PingApp.View;

namespace PingApp
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			MainPage = new NavigationPage(new MainPage());
		}

		protected override Window CreateWindow(IActivationState? activationState)
		{
			Window window = base.CreateWindow(activationState);

			window.MinimumHeight = 600;
			window.MinimumWidth = 600;

			return window;
		}
	}
}
