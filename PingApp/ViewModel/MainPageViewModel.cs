﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

using ClosedXML.Excel;

using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Storage;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Mopups.Interfaces;

using PingApp.Model;
using PingApp.View.Popup;

using UraniumUI.Dialogs;
namespace PingApp.ViewModel
{
	public partial class MainPageViewModel(IPopupNavigation popupNavigation, IDialogService dialogService) : BaseViewModel, IDisposable
	{
		const int ConvertToSeconds = 10000;

		private CancellationTokenSource? _cts;
		private CancellationToken _ct;

		private readonly IPopupNavigation _popupNavigation = popupNavigation;
		private readonly IDialogService _dialogService = dialogService;

		[ObservableProperty]
		private string? _hostName = null;
		[ObservableProperty]
		private ObservableCollection<ResultPing> _pingReplies = [];
		[ObservableProperty]
		private int _send = 0;
		[ObservableProperty]
		private int _received = 0;
		[ObservableProperty]
		private int _lost = 0;
		[ObservableProperty]
		private long min = 0;
		[ObservableProperty]
		private long max = 0;
		[ObservableProperty]
		private long avg = 0;
		[ObservableProperty]
		private int _countPackets = 4;
		[ObservableProperty]
		private int _timeSpan = 1000;
		[ObservableProperty]
		private int _ttl = 128;
		[ObservableProperty]
		private string _message = "Hello World!";
		[ObservableProperty]
		private bool _isPresented = false;
		[ObservableProperty]
		private bool _isCheckedInfinitePing = false;

		[RelayCommand]
		private async Task PingAsync()
		{
			Ping ping = new();
			PingReplies.Clear();
			Send = 0;
			Received = 0;
			Lost = 0;
			Min = 0;
			Max = 0;
			Avg = 0;

			IsBusy = true;

			try
			{
				_cts = new();
				_ct = _cts.Token;

				IPAddress[] address = await Dns.GetHostAddressesAsync(HostName!, _ct);

				PingOptions options = new(Ttl, true);
				TimeSpan timeSpan = new(TimeSpan * ConvertToSeconds);
				byte[] buffer = Encoding.ASCII.GetBytes(Message);

				if (IsCheckedInfinitePing)
				{
					while (true)
					{
						if (_ct.IsCancellationRequested) break;

						await PingRequest(ping, address, options, timeSpan, buffer);
					}
				}
				else
				{
					for (int i = 0; i < CountPackets; i++)
					{
						if (_ct.IsCancellationRequested) break;

						await PingRequest(ping, address, options, timeSpan, buffer);
					}

				}

				Send = PingReplies.Count;
				Received = PingReplies.Count(rp => rp.Status is IPStatus.Success);
				Lost = PingReplies.Count(rp => rp.Status is not IPStatus.Success);

				Min = PingReplies.Min(rp => rp.RoundtripTime);
				Max = PingReplies.Max(rp => rp.RoundtripTime);
				Avg = (long)PingReplies.Average(rp => rp.RoundtripTime);

				IsPresented = true;

			}
			catch (PingException)
			{
				await _dialogService.ConfirmAsync("Ping", "Check the data you enter");
			}
			catch (ArgumentNullException)
			{
				await _dialogService.ConfirmAsync("Argument Null", "Host name is empty");
			}
			catch (InvalidOperationException ioex)
			{
				Debug.WriteLine("InvOpeEx :" + ioex.Message);
			}
			catch (System.Net.Sockets.SocketException se)
			{
				await _dialogService.ConfirmAsync("Socket Exception", $"{se.Message} Error code: {se.ErrorCode}");
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
			finally
			{
				_cts?.Dispose();
				ping.Dispose();
				IsBusy = false;
			}
		}

		private async Task PingRequest(Ping ping, IPAddress[] address, PingOptions options, TimeSpan timeSpan, byte[] buffer)
		{
			PingReply pr = await ping.SendPingAsync(address[0]!, timeSpan, buffer, options, _ct);
			ResultPing rp = new(pr.Address, pr.Options, pr.Status, pr.RoundtripTime, pr.Buffer);
			PingReplies.Add(rp);
		}

		[RelayCommand]
		private void CancelPing()
		{
			if (IsBusy) return;

			_cts?.Cancel();
		}

		[RelayCommand]
#pragma warning disable CA1822 // Пометьте члены как статические
		private void Exit()
#pragma warning restore CA1822 // Пометьте члены как статические
		{
			Process.GetCurrentProcess().Kill();
		}

		[RelayCommand]
		private Task AboutAsync()
		{
			return _popupNavigation.PushAsync(new AboutPopupPage());
		}

		[RelayCommand]
		private async Task SaveFileAsync()
		{
			if (PingReplies.Count is 0)
			{
				await _dialogService.ConfirmAsync("Save File", "Table is empty");
				return;
			}

			using XLWorkbook workbook = new();

			IXLWorksheet worksheet = workbook.Worksheets.Add("Ping result");

			worksheet.Cell("A1").Value = "Status";
			worksheet.Cell("B1").Value = "Address";
			worksheet.Cell("C1").Value = "Ttl (ms)";
			worksheet.Cell("D1").Value = "Dont fragment";
			worksheet.Cell("E1").Value = "Buffer";

			for (int i = 0; i < PingReplies.Count; i++)
			{
				worksheet.Cell(i + 2, 1).Value = PingReplies[i].Status.ToString();
				worksheet.Cell(i + 2, 2).Value = PingReplies[i].Address?.ToString();
				worksheet.Cell(i + 2, 3).Value = PingReplies[i].Options?.Ttl;
				worksheet.Cell(i + 2, 4).Value = PingReplies[i].Options?.DontFragment ?? false ? "True" : "False";
				worksheet.Cell(i + 2, 5).Value = string.Join(string.Empty, PingReplies[i].Buffer ?? Enumerable.Empty<byte>());
			}

			worksheet.Column("A").AdjustToContents();
			worksheet.Column("B").AdjustToContents();
			worksheet.Column("C").AdjustToContents();
			worksheet.Column("D").AdjustToContents();
			worksheet.Column("E").AdjustToContents();

			worksheet.Cell(PingReplies.Count + 2, 1).Value = "Ping statistics";
			worksheet.Range(PingReplies.Count + 2, 1, PingReplies.Count + 2, 5).Merge();
			worksheet.Cell(PingReplies.Count + 2, 1).Style.Font.Bold = true;

			worksheet.Cell(PingReplies.Count + 3, 1).Value = $"Packets: sent = {Send}, received = {Received}, lost = {Lost} ({(double)Lost / Send * 100:F2}% loss)";
			worksheet.Range(PingReplies.Count + 3, 1, PingReplies.Count + 3, 5).Merge();

			worksheet.Cell(PingReplies.Count + 4, 1).Value = "Approximate round trip times in milli-seconds:";
			worksheet.Range(PingReplies.Count + 4, 1, PingReplies.Count + 4, 5).Merge();
			worksheet.Cell(PingReplies.Count + 4, 1).Style.Font.Bold = true;

			worksheet.Cell(PingReplies.Count + 5, 1).Value = $"Minimum = {Min}ms, Maximum = {Max}ms, Average = {Avg}ms";
			worksheet.Range(PingReplies.Count + 5, 1, PingReplies.Count + 5, 5).Merge();

			FileSaverResult fileSaverResult = await FileSaver.SaveAsync(FileSystem.CacheDirectory, "Result ping.xlsx", Stream.Null);

			workbook.SaveAs(fileSaverResult.FilePath);

			await Snackbar.Make("File save.", OpenFileAsync, "Open file").Show();

			async void OpenFileAsync()
			{
				await Launcher.OpenAsync(new OpenFileRequest
				{
					Title = "Open file",
					PresentationSourceBounds = Rect.Zero,
					File = new ReadOnlyFile(fileSaverResult.FilePath!, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
				});
			}
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
	}
}
