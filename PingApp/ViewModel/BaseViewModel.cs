﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace PingApp.ViewModel
{
	public abstract partial class BaseViewModel : ObservableValidator
	{
		[ObservableProperty]
		private bool _isBusy;

#pragma warning disable CA1822 // Пометьте члены как статические
		protected bool IsInternetConnecting => Connectivity.NetworkAccess is NetworkAccess.Internet;
#pragma warning restore CA1822 // Пометьте члены как статические
	}
}
